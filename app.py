from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.web import RequestHandler

from os import path

class MainPage(RequestHandler):
  def get(self):
    items = ["Главная", "Логин", "Регистрация"]
    self.render("templates/main-page.html", title="Main page's title", items=items)
    
class FavoriteBooks(RequestHandler):
  def get(self):
    favoriteBooks = {"books": 
      [{
        "name": "Harry Potter and the Philosopher's Stone",
        "year": 2001
      },
      {
        "name": "The Hobbit",
        "year": 1937
      },
      {
        "name": "JavaScript for Kids: A Playful Introduction to Programming",
        "year": 2014
      }]
    }
    self.write(favoriteBooks)

settings = {
    "static_path": path.join(path.dirname(__file__), "static"),
    "cookie_secret": "KJHKJHLKF:LFFPOFG",
    "debug": "true"
}

def init_app():
  return Application([
    (r"/", MainPage),
    (r"/get-favorite-books", FavoriteBooks)
  ], **settings )

autoreload = True

if __name__ == "__main__":
  app = init_app()
  app.listen(5000)
  IOLoop.current().start()