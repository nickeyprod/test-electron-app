function getFavoriteBooks() {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      // parse JSON
      const books = JSON.parse(responseData).books;
      // fill actually
      fillWithBooks(books);
    }
  };
  xhttp.open("GET", "http://localhost:5000/get-favorite-books", true);
  xhttp.send();
}

function fillWithBooks(books) {
  const booksContainer = document.getElementById("books-container");
  const btn = document.getElementById("btn");
  
  // clear container
  booksContainer.textContent = "";
  btn.remove();

  // fill container with parsed data
  let oneBook, bookName, bookYear;
  for (let i = 0; i < books.length; i++) {

    oneBook = document.createElement("div");
    oneBook.className = "one-book";

    bookName = document.createElement("span");
    bookName.className = "book-name"
    bookName.textContent = books[i].name;

    bookYear = document.createElement("span");
    bookYear.className = "book-year";
    bookYear.textContent = books[i].year;

    oneBook.appendChild(bookName);
    oneBook.appendChild(bookYear);

    booksContainer.appendChild(oneBook);
  }
}